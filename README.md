# Block Animate

This module is to provide block easy integration of Animate CSS.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/block_animate).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/block_animate).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Administration » Structure » Block

Block configration - Animate CSS


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Radheshyam Kumawat  - [radheymkumar](https://www.drupal.org/u/radheymkumar)
